//import
const Guru = require('./guru');
const Anak = require('./anak');

function main() {
    let data = new Guru("Bu Rina", 541211182, 50);
    let data_anak = new Anak("Rayhan", 311211183, 16, "11RPL1");

    console.log(`Data guru: bernama ${data.nama} bernomor induk pegawai ${data.nip} berumur ${data.umur}`);
    console.log(`Data anak: bernama ${data_anak.nama_anak} memiliki nis ${data_anak.nis} berumur ${data_anak.umur} kelas ${data_anak.kelas}`)

    data.mengajar('Matematika');
    data_anak.belajar('Matematika');
}

main()