class Anak {
    constructor(nama_anak, nis, umur, kelas) {
        this.nama_anak = nama_anak;
        this.nis = nis;
        this.umur = umur;
        this.kelas = kelas;
    }

    belajar(mapel) {
        console.log('kelas', this.nama_anak, 'adalah', this.kelas, 'sedang belajar', mapel);
    }
}
//export
module.exports = Anak;