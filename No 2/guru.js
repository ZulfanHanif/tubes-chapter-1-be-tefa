class Guru {
    constructor(nama, nip, umur) {
        this.nama = nama;
        this.nip = nip
        this.umur = umur;
    }

    mengajar(pelajaran) {
        console.log(this.nama, 'mengajar pelajaran', pelajaran);
    }
}

//export
module.exports = Guru;